<?php

interface UIproductos
{
	public function get();
	public function save();
	public function update();
	public function delete();

}