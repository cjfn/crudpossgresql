<?php
require_once "../models/Clientes.php";
if (empty($_POST['submit']))
{
      header("Location:" . Clientes::baseurl() . "app/clientes/list.php");
      exit;
}
$args = array(
    'id'        => FILTER_VALIDATE_INT,
    'nombre'  => FILTER_SANITIZE_STRING,
    'apellido'  => FILTER_SANITIZE_STRING,
    'correo'  => FILTER_SANITIZE_STRING,
    'telefono'  => FILTER_VALIDATE_INT,
);
 
$post = (object)filter_input_array(INPUT_POST, $args);
 
if( $post->id === false )
{
    header("Location:" . Clientes::baseurl() . "app/clientes/list.php");
}
 
$db = new Database;
$clientes = new Clientes($db);
$clientes->setId($post->id);
$clientes->setnombre($post->nombre);
$clientes->setapellido($post->apellido);
$clientes->settelefono($post->telefono);
$clientes->setcorreo($post->correo);
$clientes->update();
header("Location:" . Clientes::baseurl() . "app/clientes/list.php");