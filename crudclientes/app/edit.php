<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Listado de Clientes</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" media="screen" title="no title" charset="utf-8">
</head>
<body>
    <?php
    require_once "../models/Clientes.php";
    $codcliente = filter_input(INPUT_GET, 'cliente', FILTER_VALIDATE_INT);
    if( ! $codcliente )
    {
        header("Location:" . Clientes::baseurl() . "app/list.php");
    }
    $db = new Database;
    $newCliente = new Clientes($db);
    $newCliente->setId($codcliente);
    $cliente = $newCliente->get();
    $newCliente->checkUser($cliente);
    ?>
    <div class="container">
        <div class="col-lg-12">
            <h2 class="text-center text-primary">Editar cliente <?php echo $cliente->nombre ?></h2>
            <form action="<?php echo Clientes::baseurl() ?>app/clientes/update.php" method="POST">
                <div class="form-group">
                    <label for="nombre">Nombre:</label>
                    <input type="text" name="nombre" value="<?php echo $cliente->nombre ?>" class="form-control" id="nombre" placeholder="nombre">
                </div>
                <div class="form-group">
                    <label for="apellido">apellido:</label>
                    <input type="apellido" name="apellido" value="<?php echo $cliente->apellido ?>" class="form-control" id="apellido" placeholder="apellido">
                </div>
                <div class="form-group">
                    <label for="correo">correo:</label>
                    <input type="correo" name="correo" value="<?php echo $cliente->correo ?>" class="form-control" id="correo" placeholder="correo">
                </div>  
                 <div class="form-group">
                    <label for="telefono">telefono</label>
                    <input type="telefono" name="telefono" value="<?php echo $cliente->telefono ?>" class="form-control" id="telefono" placeholder="telefono">
                </div>                             
                <input type="hidden" name="codcliente" value="<?php echo $cliente->codcliente ?>" />
                <input type="submit" name="submit" class="btn btn-default" value="Actualiza cliente" />
            </form>
        </div>
    </div>
</body>
</html>