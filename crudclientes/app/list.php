<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Listado de clientes</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" media="screen" title="no title" charset="utf-8">
    </head>
    <body>
        <?php
        require_once "../models/Clientes.php";
        $db = new Database;
        $cliente = new Clientes($db);
        $clientes = $cliente->get();
        ?>
        <div class="container">
            <div class="col-lg-12">
                <h2 class="text-center text-primary">Listado de clientes</h2>
                <div class="col-lg-1 pull-right" style="margin-bottom: 10px">
                    <a class="btn btn-info" href="<?php echo Clientes::baseurl() ?>app/add.php">Agregar Nuevo Cliente</a>
                </div>
                <?php
                if( ! empty( $clientes ) )
                {
                ?>
                <table class="table table-striped">
                    <tr>
                        <th>Id</th>
                        <th>nobre</th>
                        <th>apellido</th>
                        <th>telefono</th>
                        <th>correo</th>
                        <th>fecha creacion</th>
                        <th>opciones</th>
                    </tr>
                    <?php foreach( $clientes as $cliente )
                    {
                    ?>
                        <tr>
                            <td><?php echo $cliente->codcliente ?></td>
                            <td><?php echo $cliente->nombre ?></td>
                            <td><?php echo $cliente->apellido ?></td>
                            <td><?php echo $cliente->telefono ?></td>
                            <td><?php echo $cliente->correo ?></td>
                            <td><?php echo $cliente->fecha_create ?></td>
                            <td>
                                <a class="btn btn-info" href="<?php echo Clientes::baseurl() ?>app/edit.php?cliente=<?php echo $cliente->codcliente ?>">Edit</a> 
                                <a class="btn btn-info" href="<?php echo Clientes::baseurl() ?>app/delete.php?cliente=<?php echo $cliente->codcliente ?>">Delete</a>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                </table>
                <?php
                }
                else
                {
                ?>
                <div class="alert alert-danger" style="margin-top: 100px">Ningun Cliente registrado</div>
                <?php
                }
                ?>
            </div>
        </div>
    </body>
</html>