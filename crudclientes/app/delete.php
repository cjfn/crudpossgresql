<?php
require_once "../models/Clientes.php";
$db = new Database;
$cliente = new Clientes($db);
$id = filter_input(INPUT_GET, 'cliente', FILTER_VALIDATE_INT);
 
if( $id )
{
    $cliente->setId($id);
    $cliente->delete();
}
header("Location:" . Clientes::baseurl() . "app/cliente/list.php");