<?php
require_once("../db/Database.php");
require_once("../interfaces/ICliente.php");
 
class Clientes implements ICliente
{
	private $con;
    private $codcliente;
    private $nombre;
    private $apellido;
    private $correo;
    private $telefono;
 
	public function __construct(Database $db)
	{
		$this->con = new $db;
	}
 
    public function setId($codcliente)
    {
        $this->codcliente = $codcliente;
    }
 
    public function setnombre($nombre)
    {
        $this->nombre = $nombre;
    }
 
    public function setapellido($apellido)
    {
        $this->apellido = $apellido;
    }

        public function setcorreo($correo)
    {
        $this->correo = $correo;
    }

     public function setfecha($fecha_create)
    {
        $this->fecha_create = $fecha_create;
    }
 
	//insertamos usuarios en una tabla con postgreSql
	public function save()
	{
		try{
			$query = $this->con->prepare('INSERT INTO clientes (nombre, apellido, correo, telefono, fecha_create) values (?,?,?,?,?)');
            $query->bindParam(1, $this->nombre, PDO::PARAM_STR);
			$query->bindParam(2, $this->apellido, PDO::PARAM_STR);
            $query->bindParam(3, $this->correo, PDO::PARAM_STR);
            $query->bindParam(4, $this->telefono, PDO::PARAM_INT);
            $query->bindParam(5, $this->fecha_create (date("Y-m-d")), PDO::PARAM_STR);
			$query->execute();
			$this->con->close();
		}
        catch(PDOException $e)
        {
	        echo  $e->getMessage();
	    }
	}
 
    public function update()
	{
		try{
			$query = $this->con->prepare('UPDATE clientes SET nombre = ?, apellido = ?, correo = ?, telefono = ? WHERE codcliente = ?');
			$query->bindParam(1, $this->nombre, PDO::PARAM_STR);
			$query->bindParam(2, $this->apellido, PDO::PARAM_STR);
            $query->bindParam(3, $this->correo, PDO::PARAM_STR);
            $query->bindParam(4, $this->telefono, PDO::PARAM_INT);
            $query->bindParam(5, $this->codcliente, PDO::PARAM_INT);
			$query->execute();
			$this->con->close();
		}
        catch(PDOException $e)
        {
	        echo  $e->getMessage();
	    }
	}
 
	//obtenemos usuarios de una tabla con postgreSql
	public function get()
	{
		try{
            if(is_int($this->codcliente))
            {
                $query = $this->con->prepare('SELECT * FROM clientes WHERE codcliente = ?');
                $query->bindParam(1, $this->codcliente, PDO::PARAM_INT);
                $query->execute();
    			$this->con->close();
    			return $query->fetch(PDO::FETCH_OBJ);
            }
            else
            {
                $query = $this->con->prepare('SELECT * FROM clientes');
    			$query->execute();
    			$this->con->close();
    			return $query->fetchAll(PDO::FETCH_OBJ);
            }
		}
        catch(PDOException $e)
        {
	        echo  $e->getMessage();
	    }
	}

    public function gettipocliente()
    {
    try{
                $query = $this->con->prepare('SELECT * FROM tipo_cliente');
                $query->execute();
                $this->con->close();
                return $query->fetchAll(PDO::FETCH_OBJ);
        }

                catch(PDOException $e)
        {
            echo  $e->getMessage();
        }
            
        
    }
 
    public function delete()
    {
        try{
            $query = $this->con->prepare('DELETE FROM clientes WHERE codcliente = ?');
            $query->bindParam(1, $this->codcliente, PDO::PARAM_INT);
            $query->execute();
            $this->con->close();
            return true;
        }
        catch(PDOException $e)
        {
            echo  $e->getMessage();
        }
    }
 
    public static function baseurl()
    {
         return stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://' . $_SERVER['HTTP_HOST'] . "/crudclientes/";
    }
 
    public function checkUser($cliente)
    {
        if( ! $cliente )
        {
            header("Location:" . Clientes::baseurl() . "/app/list.php");
        }
    }
}