<?php
interface ICliente{
    public function get();
    public function save();
    public function update();
    public function delete();
    public function gettipocliente();
}