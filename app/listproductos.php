<?php
	include('header.php');
	miheader();

        require_once "../models/productos.php";
        $db = new Database;
        $producto = new Producto($db);
        $productos = $producto->get();
        ?>
        <div class="container">
            <div class="col-lg-12">
                <h2 class="text-center text-primary">Product List</h2>
                <div class="col-lg-1 pull-right" style="margin-bottom: 10px">
                    <a class="btn btn-info" href="http://mantenimiento.wbinnovacionreal.com/app/addproducto.php">Agregar Producto</a>
                </div>
                <?php
                if( ! empty( $productos ) )
                {
                ?>
                <table class="table table-striped">
                    <tr>
                        <th>codigo</th>
                        <th>nombre</th>
                        <th>cantidad</th>
                        <th>precio</th>
                        <th>fecha</th>
                        <th>proveedor</th>
                        <th>opciones</th>
                    </tr>
                    <?php foreach( $productos as $producto )
                    {
                    ?>
                        <tr>
                            <td><?php echo $producto->codproducto ?></td>
                            <td><?php echo $producto->nombre ?></td>
                            <td><?php echo $producto->cantidad ?></td>
                            <td><?php echo $producto->precio ?></td>
                            <td><?php echo $producto->fecha_create ?></td>
                            <td><?php echo $producto->proveedor ?></td>
                            <td>
                                <a class="btn btn-info" href="<?php echo Producto::baseurl() ?>app/editproducto.php?producto=<?php echo $producto->codproducto ?>">Edit</a> 
                                <a class="btn btn-info" href="<?php echo Producto::baseurl() ?>app/delete.php?producto=<?php echo $producto->codproducto ?>">Delete</a>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                </table>
                <?php
                }
                else
                {
                ?>
                <div class="alert alert-danger" style="margin-top: 100px">Ningun producto ha sido registrado</div>
                <?php
                }
                ?>
            </div>
        </div>
    </body>
</html>