<?php
require_once "../models/productos.php";
if (empty($_POST['submit']))
{
      header("Location:" . User::baseurl() . "app/listproductos.php");
      exit;
}
 
$args = array(
    'nombre'  => FILTER_SANITIZE_STRING,
    'cantidad'  => FILTER_VALIDATE_INT,
    'precio' => FILTER_SANITIZE_NUMBER_FLOAT,
    'fecha_create' => FILTER_SANITIZE_STRING,
    
);
 
$post = (object)filter_input_array(INPUT_POST, $args);
 
$db = new Database;
$producto = new productos($db);
$producto->setnombre($post->nombre);
$user->setcantidad($post->cantidad);
$user->setprecio($post->precio);
$user->setfecha($post->fecha_create);
$user->save();

header("Location:" . User::baseurl() . "app/list.php");
echo "<script language='javascript'> alert('dato ingresado'); </script>";