<?php
require_once "../models/productos.php";
$db = new Database;
$producto = new productos($db);
$codproducto = filter_input(INPUT_GET, 'producto', FILTER_VALIDATE_INT);
 
if( $codproducto )
{
    $producto->setId($codproducto);
    $producto->delete();
}
header("Location:" . productos::baseurl() . "app/listproducto.php");