<?php
require_once "../models/productos.php";
if (empty($_POST['submit']))
{
      header("Location:" . productos::baseurl() . "app/listproducto.php");
      exit;
}
$args = array(
    'codproducto'        => FILTER_VALIDATE_INT,
    'nombre'  => FILTER_SANITIZE_STRING,
    'cantidad'  => FILTER_VALIDATE_INT,
    'precio'  => FILTER_SANITIZE_NUMBER_FLOAT,
    'fecha_create'  => FILTER_SANITIZE_STRING,
);
 
$post = (object)filter_input_array(INPUT_POST, $args);
 
if( $post->id === false )
{
    header("Location:" . productos::baseurl() . "app/listproducto.php");
}
 
$db = new Database;
$producto = new productos($db);
$producto->setId($post->codproducto);
$producto->setnombre($post->nombre);
$producto->setprecio($post->precio);
$producto->setfecha($post->fecha_create);
$producto->update();
header("Location:" . producto::baseurl() . "app/listproducto.php");