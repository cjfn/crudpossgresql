<?php
	include('header.php');
	miheader();
require_once "../models/productos.php";
    $codproducto = filter_input(INPUT_GET, 'producto', FILTER_VALIDATE_INT);
    if( ! $codproducto )
    {
        header("Location:" . productos::baseurl() . "app/listproductos.php");
    }
    $db = new Database;
    $newProducto = new producto($db);
    $newProducto->setId($codproducto);
    $producto = $NewProducto->get();
    $newProducto->checkUser($producto);
    ?>
    <div class="container">
        <div class="col-lg-12">
            <h2 class="text-center text-primary">Edit user <?php echo $producto->username ?></h2>
            <form action="<?php echo producto::baseurl() ?>app/updateproducto.php" method="POST">
                <div class="form-group">
                    <label for="nombre">nombre</label>
                    <input type="text" name="nombre" value="<?php echo $producto->nombre ?>" class="form-control" id="nombre" placeholder="nombre">
                </div>
                <div class="form-group">
                    <label for="cantidad">cantidad</label>
                    <input type="number" name="cantidad" value="<?php echo $producto->cantidad ?>" class="form-control" id="cantidad" placeholder="cantidad">
                </div>
                <div class="form-group">
                    <label for="precio">precio</label>
                    <input type="text" name="precio" value="<?php echo $producto->precio ?>" class="form-control" id="precio" placeholder="precio">
                </div>
                <div class="form-group">
                    <label for="fecha_create">fecha</label>
                    <input type="date" name="fecha_create" value="<?php echo $producto->fecha_create ?>" class="form-control" id="fecha_create" placeholder="fecha_create">
                </div>

                <input type="hidden" name="codproducto" value="<?php echo $producto->codproducto ?>" />
                <input type="submit" name="submit" class="btn btn-default" value="Update user" />
            </form>
        </div>
    </div>
</body>
</html>
