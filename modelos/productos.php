<?php
require('../database/Database.php');
require('../interfaces/UIproductos.php');

class Producto implements UIproductos
{
	private $con;
	private $codproducto;
	private $nombre;
	private $cantidad;
	private $precio;
	private $fecha_create;


	public function __construct(Database $db)
	{
		$this->con=$db;
	}

	public function setId($codproducto)
	{
		$this->codproducto=$codproducto;

	}


	public function setnombre($nombre)
	{
		$this->nombre=$nombre;
		
	}


	public function setcantidad($cantidad)
	{
		$this->cantidad=$cantidad;
		
	}


	public function setprecio($precio)
	{
		$this->precio=$precio;
		
	}


	public function setfecha($fecha_create)
	{
		$this->fecha_create=$fecha_create;
		
	}


	public function save()
	{

		try
		{
		$query=$this->con->prepare('insert into productos(nombre,cantidad,precio,fecha_create) values (?,?,?,?)');
		$query->bindParam(1,$this->nombre,PDO::PARAM_STR);
		$query->bindParam(2,$this->cantidad,PDO::PARAM_INT);
		$query->bindParam(3,$this->precio,PDO::PARAM_INT);
		$query->bindParam(4,$this->fecha_create,PDO::PARAM_STR);
		$query->execute();
		$this->con->close();

		}
		catch(PDOexeption $e)
		{
			echo $e->getMessage();
		}
		
	}

	public function get()
	{
		try
		{
			if(is_int($this->codproducto))
			{
				$query=$this->con->prepare('SELECT * FROM productos where codproducto = ?');
				$query->bindParam(1,$this->codproducto,PDO::PARAM_INT);
				$query->execute();
				$this->con->close();
				return $query->fetch(PDO::FETCH_OBJ);

			}
			else
			{
				$query=$this->con->prepare('SELECT * FROM productos');
				$query->execute();
				$this->con->close();
				return $query->fetch(PDO::FETCH_OBJ);
			}
			
		}
		catch(PDOexeption $e)
		{
			$e->getMessage();
		}
	}

	

	public function update()
	{
		try
		{
		$query=$this->con->prepare('UPDATE productos SET nombre=?, cantidad=?, precio=?, fecha_create=? where codproducto=?');
		$query->bindParam(1,$this->nombre,PDO::PARAM_STR);
		$query->bindParam(2,$this->cantidad,PDO::PARAM_INT);
		$query->bindParam(3,$this->precio,PDO::PARAM_INT);
		$query->bindParam(4,$this->fecha_create,PDO::PARAM_STR);
		$query->bindParam(5,$this->codproducto,PDO::PARAM_INT);
		$query->execute();
		$this->con->close();		
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}

	public function delete()
	{
		try
		{
		$query=$this->con->prepare('delete from producto where codproducto=?');
		$query->bindParam(1,$this->codproducto,PDO::PARAM_INT);
		$query->execute();
		$this->con->close();
		}
		catch(PDOexeption $e)
		{
			echo $e->getMessage();
		}
	}

	 public static function baseurl()
    {
         return stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://' . $_SERVER['HTTP_HOST'] . "/";
    }
 
    public function checkUser($user)
    {
        if( ! $user )
        {
            header("Location:" . User::baseurl() . "app/list.php");
        }
    }
}